#!/usr/bin/python3

'''
 Copyright (C) 2022, Raphaël . Droz + floss @ gmail DOT com
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later versi

 This script hits the JSON REST API endpoints of a WordPress instance and iterates over the pages
 of contents  (post type(s) can be specified).
 For each of them, links are extracted, parsed, tested and the result of every remote resource is cached.

 From this, a CSV report is generated containing every "broken" (or redirected) resource, their final target and
 the page they were encountered.
'''

import sys
import os
import re
import csv
import json
import concurrent.futures
import argparse
import logging
from operator import itemgetter
from urllib.parse import urljoin
import requests
from requests.exceptions import SSLError, HTTPError, ConnectionError, MissingSchema, Timeout, InvalidSchema, TooManyRedirects, InvalidURL
import bs4

# The links actually extracted during this run
extracted_links = []

# A list of all previous links encountered, there final target
# is preserved
cached_links = []

# A dict created at runtime from the above
tested_links = {}

domain = None
output_file = None
PER_PAGE = 50
CHECKPOINT_FILE = 'checkpoint-%s.json'
POST_TYPE = ''

STATE = {"types": {}, # Dict of dict of {"page": 0, "num_pages": 0
         "domain": '',
         "out": '',
         "links": []}

headers = {
    # Warning: This may trigger redirection to the mobile version of some websites (Eg: m.youtube.com or netflix intent:// links)
    'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Mobile Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-language': 'en-US,en;q=0.9,tr;q=0.8',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'same-origin',
    'TE': 'trailers',
    'Upgrade-Insecure-Requests': '1'
}

session = requests.Session()

def getLinks(rendered_content):
    soup = bs4.BeautifulSoup(rendered_content, 'html.parser')
    return [link['href'] for link in soup('a') if 'href' in link.attrs]

def is_redirect(code):
    return isinstance(code, int) and code >= 300 and code < 400

def getStatusCode(link, data, timeout=5):
    if link == '':
        return link, 500, '', data
    elif 'ftp://' in link:
        return link, 200, '', data
    elif 'mailto:' in link:
        return link, 200, '', data
    elif 'tel:' in link:
        return link, 200, '', data
    elif 'intent://' in link:
        return link, 200, '', data

    if '://' not in link:
        link = urljoin(f'https://{domain}/', link)

    if link in tested_links:
        code = tested_links[link][0]
        if not is_redirect(code):
            return link, code, '', data

        if len(tested_links[link]) == 2 and tested_links[link][1] != '':
            return link, code, tested_links[link][1], data

        # If we stored an 301 but failed to store the final destination, try it again
        logging.warning(f"existing target_url for {link}")
        logging.warning(tested_links[link])

    try:
        r = session.head(link, timeout=timeout)
    except (SSLError, HTTPError, ConnectionError, MissingSchema, Timeout, InvalidSchema, InvalidURL, TooManyRedirects) as errh:
        logging.warning(f"Error in URL {link}: {errh.__class__.__name__}")
        tested_links[link] = [errh.__class__.__name__]
        return link, errh.__class__.__name__, '', data
    else:
        tested_links[link] = [r.status_code]
        # print(f"######## [else] clause for {link} {r.status_code}")

        if re.search(r'(youtube|instagram|twitter|facebook|imdb)\.com|twitch\.tv|youtu\.be|netflix\.com', link):
            return link, r.status_code, '', data
        try:
            get_request = session.get(link, timeout=timeout)
            final_url = get_request.url
        except (HTTPError, ConnectionError, MissingSchema, Timeout, TooManyRedirects, InvalidSchema) as errh:
            final_url = ''
        return link, r.status_code, final_url, data

def future_completed_callback(future):
    url, code, final_url, payload = future.result()
    info = {
        "id": payload['id'],
        "source": payload['link'],
        "target": url,
        "code": code,
        "final_target": ''
    }
    if is_redirect(code) and final_url != '':
        info['final_target'] = final_url
    cached_links.append(info)
    extracted_links.append(info)

def processMarkup(markup, data, executor):
    fut = None
    futures = []
    post_links = list(set(getLinks(markup)))

    for url in post_links:
        fut = executor.submit(getStatusCode, url, data)
        fut.add_done_callback(future_completed_callback)
        futures.append(fut)
    return futures

def checkpoint(num = None):
    if POST_TYPE and num:
        STATE['types'][POST_TYPE]['page'] = num

    logging.debug("Saving checkpoint")
    STATE['links'] = sorted(cached_links, key=lambda k: k['id'])
    with open(CHECKPOINT_FILE % domain, 'w') as f:
        f.write(json.dumps(STATE))

def prepare_crawl(post_type, start, end):
    logging.info(f"Crawling {domain}'s {post_type} from page #{start} to page #{end}.")

    POST_TYPE = post_type
    STATE['types'][post_type] = {'page': start, 'num_pages': end}
    STATE['domain'] = domain
    STATE['out'] = output_file if isinstance(output_file, str) else output_file.name

    data_page_list = {}
    for i in range(start, end):
        data_page_list[i] = f'https://{domain}/wp-json/wp/v2/{post_type}?per_page={PER_PAGE}&page={i}'

    logging.debug(f"{start}, {end}")
    crawl(data_page_list)
    checkpoint(end)

def crawl(urls):
    with concurrent.futures.ThreadPoolExecutor(max_workers = 4) as executor:
        futures = []

        i = list(urls.keys())[0]
        for i, url in urls.items():
            # done = len(list(concurrent.futures.as_completed(futures)))
            logging.info("% 4d links checked. Processing page % 3d/% 3d." % (len(futures), i, list(urls.keys())[-1]))
            post_data = session.get(url).json()
            for data in post_data:
                if 'link' not in data:
                    logging.warning(f"No <link> found at {url}.")
                    print(data)
                    continue
                logging.debug(data["link"])
                new_futures = processMarkup(data["content"]["rendered"], data, executor)
                for f in new_futures:
                    futures.append(f)
                    if len(futures) % 40 == 0:
                        checkpoint(i)

            if i % 5 == 0:
                checkpoint(i)
        else:
            checkpoint(i)


def output():
    if len(extracted_links) == 0:
        logging.warning("Nothing found")
        return

    global output_file
    sorted_links = sorted(extracted_links, key=lambda k: k['id'])
    sorted_links = list(filter(lambda x: not isinstance(x['code'], int) or x['code'] != 200, sorted_links))

    with open(output_file, 'w+',  newline='', encoding='utf-8') if isinstance(output_file, str) else output_file as stream:
        csvwriter = csv.DictWriter(stream, lineterminator='\n', fieldnames=list(sorted_links[0].keys()))
        csvwriter.writeheader()
        csvwriter.writerows(sorted_links)
        if isinstance(output_file, str):
            logging.info("Report saved in file %s" % stream.name)

def select_output_file(proposed):
    global output_file

    if proposed == '<stdout>':
        output_file = sys.stdout
    elif proposed == '<stderr>':
        output_file = sys.stderr
    elif isinstance(output_file, str) and os.path.exists(proposed) and os.path.getsize(proposed) > 0:
        for i in range(1, 10):
            proposed = re.sub(r'(#[0-9]*)?(\.[^.]+)$', r'#%02d\1' % i, proposed)
            if not os.path.exists(proposed) or os.path.getsize(proposed) == 0:
                break
    output_file = proposed

def get_num_pages(post_type):
    page_list = session.get(f'https://{domain}/wp-json/wp/v2/%s?per_page=%d' % (post_type, PER_PAGE))
    try:
        return int(page_list.headers['x-wp-totalpages'])
    except KeyError:
        return 0

def begin(args, post_type):
    end_page = get_num_pages(post_type)
    if args.limit:
        end_page = min(end_page, args.limit)
    end_page += 1
    prepare_crawl(post_type, 1, end_page)

def warm(args):
    global domain, cached_links

    data = json.loads(args.resume.read())
    STATE['types'] = data['types'] if 'types' in data else {'posts': {}}

    if 'page' in data:
        STATE['types']['posts']['page'] = data['page']
    if 'num_pages' in data:
        STATE['types']['posts']['num_pages'] = data['num_pages']

    domain = data['domain']
    cached_links = [{"target" if k == 'Broken Link' else 'code' if k == 'Status Code' else 'id' if k == 'Post ID' else 'source' if k == 'Post Link' else k:v for k,v in element.items()} for element in data['links']]
    cached_links = [k for k in cached_links if k['code'] not in ['ConnectTimeout', 'ReadTimeout']]

    for one_link in cached_links:
        if 'target' in one_link and one_link['target'] not in tested_links and one_link['code'] not in ['ConnectTimeout', 'ReadTimeout']:
            tested_links[one_link['target']] = [one_link['code'], one_link['final_target'] if 'final_target' in one_link else '']

    select_output_file(data['out'] if 'out' in data else args.output)

    logging.info("Reloaded %d items and %d distinct cached URL codes" % (len(cached_links), len(tested_links)))

def resume(args, post_types):
    posts_type_to_proceed = post_types if len(post_types) > 0 else STATE['types'].keys()
    for post_type in posts_type_to_proceed:
        try:
            start = STATE['types'][post_type]['page']
        except KeyError:
            start = 1

        end_page = get_num_pages(post_type) + 1
        if args.limit:
            end_page = start + args.limit
        if start == end_page:
            start -= 1
        prepare_crawl(post_type, start, end_page)

def enable_debug(verbosity, debug):
    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    level = levels[min(len(levels)-1, verbosity)]

    logging.basicConfig()
    logging.getLogger().setLevel(level)
    if verbosity >= 2 or debug:
        requests_log = logging.getLogger("requests.packages.urllib3")
        requests_log.setLevel(level)
        requests_log.propagate = True

def main():
    global domain

    parser = argparse.ArgumentParser(description="Broken links extractor")
    parser.add_argument('domain', nargs=None, type=str, help='Domain.')
    parser.add_argument('-t', '--types', type=str, help='Limit to these post types', default='')
    parser.add_argument('-l', '--limit', type=int, help='Limit to that many pages')
    parser.add_argument('-o', '--output', type=argparse.FileType('w+'), default=sys.stdout, help='Output CSV file.')
    parser.add_argument('-r', '--resume', type=argparse.FileType('r'), help='Resume from a JSON checkpoint file.')
    parser.add_argument('-v', '--verbose', action='count', default=1, help='More verbose')
    parser.add_argument("-d", "--debug", action='store_true', help="Debugging output")
    parser.add_argument("-u", "--url", help="Use this single URL as crawl starting-point.")

    args = parser.parse_args()
    enable_debug(args.verbose, args.debug)
    post_types = list(set(filter(None, args.types.split(','))))
    domain = args.domain
    session.headers = headers
    session.headers.update({'referer': f'https://{domain}'})

    if args.resume:
        warm(args)
        resume(args, post_types)
    elif args.url:
        select_output_file(args.output)
        crawl({1: args.url})
    else:
        select_output_file(args.output)
        if len(post_types) == 0:
            post_types = ['posts', 'pages']
        for t in post_types:
            begin(args, t)

    output()

if __name__ == '__main__':
    main()
